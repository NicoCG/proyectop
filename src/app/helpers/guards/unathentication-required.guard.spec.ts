import { TestBed, async, inject } from '@angular/core/testing';

import { UnathenticationRequiredGuard } from './unathentication-required.guard';

describe('UnathenticationRequiredGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UnathenticationRequiredGuard]
    });
  });

  it('should ...', inject([UnathenticationRequiredGuard], (guard: UnathenticationRequiredGuard) => {
    expect(guard).toBeTruthy();
  }));
});
