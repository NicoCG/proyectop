import { Component, OnInit } from "@angular/core";
import { SecurityService } from "src/app/services/security.service";
import { Subscription } from 'rxjs';
import { UserModel } from 'src/app/models/user.model';

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"]
})
export class NavbarComponent implements OnInit {

  
  constructor(private secService: SecurityService) {}
  userInfo: UserModel;
  userLogged:boolean = false;
  userAsesor:boolean = false;
  userClient: boolean = false;
  userAdmin: boolean = false;
  userName: String;
  userCall: UserModel;

  subscription: Subscription;
  ngOnInit() {
    this.verifyUserSession();
  }

  verifyUserSession() {
    this.subscription = this.secService.getUserInfo().subscribe(user => {
      this.userInfo = user;
      this.updateInfo();
    });
  }
  updateInfo(){
    let msg = "Welcome: ";
    this.userLogged = this.userInfo.isLogged;
    this.userAsesor = this.userInfo.isAsesor;
    this.userClient = this.userInfo.isClient;
    this.userAdmin = this.userInfo.isAdmin;
    this.userName = `${msg} ${this.userInfo.firstname} ${this.userInfo.lastname} ${this.userInfo.email} ${this.userInfo.phone} ${this.userInfo.rol}`;
    
  }
  //this.userName = `${msg} ${this.userInfo.firstname} ${this.userInfo.lastname} ${this.userInfo.email} ${this.userInfo.phone} ${this.userInfo.rol}`;

}
