import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { observable, BehaviorSubject, Observable } from "rxjs";
import { UserModel } from '../models/user.model';

@Injectable({
  providedIn: "root"
})
export class SecurityService {
  url: String ="http://localhost:3000/api/users/";

  userInfo = new BehaviorSubject<UserModel>(new UserModel());

  constructor(private http: HttpClient) {
    this.verifyUserInSession();
  }

  verifyUserInSession() {
    let session = localStorage.getItem("activeUser");
    if(session != undefined){
      this.userInfo.next(JSON.parse(session));
    }
  }

  getUserInfo() {
    return this.userInfo.asObservable();
  }

  loginUser(username: String, pass: String):Observable<UserModel> {
    return this.http.post<UserModel>(`${this.url}login?include=Admin`,
    {
      email:username,
      password:pass
    },
    {headers: new HttpHeaders({
      "content-type":"application/json"

    })})
  }
  saveLoginInfo(user: UserModel){

    user.isLogged = true;
    
    this.userInfo.next(user);
    localStorage.setItem("activeUser", JSON.stringify(user));

    if(user.rol==1){
      user.isAdmin = true;
      
    
           
    }else{
      if(user.rol==2){
        user.isAsesor= true;
       
      }else{
        if(user.rol==3){
          user.isClient = true;
        }
      }
    }
    

  }

  isActiveSession(){
    return this.userInfo.getValue().isLogged;
  }
  isAdminSession(){
    return this.userInfo.getValue().isAdmin;
  }

  logoutUser(){
    localStorage.removeItem("activeUser");
    this.userInfo.next(new UserModel());
  }




}
