import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { UserModel } from "../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  url = "http://localhost:3000/api/Users";



  saveNewUser(userList: UserModel): Observable<UserModel> {
    return this.http.post<UserModel>(`${this.url}`, userList, {
      headers: new HttpHeaders({
        "content-type": "application/json"
      })
    });
  }

}

