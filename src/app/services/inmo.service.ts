import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { InmoModel } from "../models/inmo.model";
import { throws } from "assert";

@Injectable({
  providedIn: "root"
})
export class InmoService {
  url = "http://localhost:3000/api/Inmos";

  constructor(private http: HttpClient) {}



  loadAllInmo(): Observable<InmoModel[]> {
    return this.http.get<InmoModel[]>(`${this.url}`);
  }
  getInmoById(_id: String):Observable<InmoModel>{
    return this.http.get<InmoModel>(`${this.url}/${_id}`);
  }

  saveNewInmo(inmoList: InmoModel): Observable<InmoModel> {
    return this.http.post<InmoModel>(`${this.url}`, inmoList, {
      headers: new HttpHeaders({
        "content-type": "application/json"
      })
    });
  }

  updateNewInmo(inmoList: InmoModel):Observable<InmoModel>{
    return this.http.put<InmoModel>(`${this.url}`,inmoList,{
      headers: new HttpHeaders({
        "content-type": "application/json"
      })
    });

  }

  deleteNewInmo(id: String):Observable<InmoModel>{
    return this.http.delete<InmoModel>(`${this.url}/${id}`);
  }
}
