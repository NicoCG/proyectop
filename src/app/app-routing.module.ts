import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './public/home/home.component';
import { PageNotFoundComponent } from './public/page-not-found/page-not-found.component';
import { AuthenticationRequiredGuard } from './helpers/guards/authentication-required.guard';
import { AdminGuard } from './helpers/guards/admin.guard';


const routes: Routes = [
  {
    path:'home',
    component: HomeComponent
  },
  
  {
    path:'security',
    loadChildren:'./modules/security/security.module#SecurityModule'
    
  },
  {
    path:'inmo',
    loadChildren:'./modules/parameters/inmo-admin/inmo-admin.module#InmoAdminModule',
    canActivate: [AuthenticationRequiredGuard],
    

  },
  {
    path:'',
    pathMatch: 'full',
    redirectTo: '/home'
  },
  
  {
    path:'**',
    component: PageNotFoundComponent
    
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
