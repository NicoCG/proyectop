import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InmoCreatorComponent } from './inmo-creator.component';

describe('InmoCreatorComponent', () => {
  let component: InmoCreatorComponent;
  let fixture: ComponentFixture<InmoCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InmoCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InmoCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
