import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InmoModel } from 'src/app/models/inmo.model';
import { InmoService } from 'src/app/services/inmo.service';
import { Subscriber } from 'rxjs';
import { Router } from '@angular/router';

declare let openPlatformModalMessage: any;

@Component({
  selector: 'app-inmo-creator',
  templateUrl: './inmo-creator.component.html',
  styleUrls: ['./inmo-creator.component.css']
})
export class InmoCreatorComponent implements OnInit {
  
  list: InmoModel = {
    
    city:null,
    departament:null,
    value:null,
    address:null,
    property:null,
    offer: null

  }
  frmValidator: FormGroup;

  constructor(private fb: FormBuilder, private inmoService: InmoService, private router: Router) { }


  ngOnInit() {
    this.frmGenerator();
  }
  get fv(){
    return this.frmValidator.controls;
  }
  frmGenerator(){
    this.frmValidator = this.fb.group({
      city:['',[Validators.required]],
      departament:['',Validators.required],
      address:['',[Validators.required]],
      value:['',[Validators.required]],
      property:['',[Validators.required]],
      offer:['',[Validators.required]]
    })
  }

  saveInmo():void {
    
    this.inmoService.saveNewInmo(this.list).subscribe(item=>{
      openPlatformModalMessage("Data stored successfully.");
      this.router.navigate(['/inmo/list']);
    }
      
      )


    
  }

}
