import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InmoCreatorComponent } from './inmo-creator/inmo-creator.component';
import { InmoEditorComponent } from './inmo-editor/inmo-editor.component';
import { InmoListComponent } from './inmo-list/inmo-list.component';


const routes: Routes = [
  {
    path:'creator',
    component: InmoCreatorComponent
  },
  {
    path:'editor/:id',
    component:InmoEditorComponent
  },
  {
    path:'list',
    component:InmoListComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/list'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InmoAdminRoutingModule { }
