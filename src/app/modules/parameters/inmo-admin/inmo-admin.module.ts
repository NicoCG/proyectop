import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InmoAdminRoutingModule } from './inmo-admin-routing.module';
import { InmoCreatorComponent } from './inmo-creator/inmo-creator.component';
import { InmoEditorComponent } from './inmo-editor/inmo-editor.component';
import { InmoListComponent } from './inmo-list/inmo-list.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";


@NgModule({
  declarations: [InmoCreatorComponent, InmoEditorComponent, InmoListComponent],
  imports: [
    CommonModule,
    InmoAdminRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class InmoAdminModule { }
