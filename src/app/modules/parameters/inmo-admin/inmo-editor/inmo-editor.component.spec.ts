import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InmoEditorComponent } from './inmo-editor.component';

describe('InmoEditorComponent', () => {
  let component: InmoEditorComponent;
  let fixture: ComponentFixture<InmoEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InmoEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InmoEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
