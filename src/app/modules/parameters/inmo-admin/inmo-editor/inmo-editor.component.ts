import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { InmoService } from 'src/app/services/inmo.service';
import { Router, ActivatedRoute } from '@angular/router';
import { throws } from 'assert';
import { InmoModel } from 'src/app/models/inmo.model';

declare let openPlatformModalMessage: any;

@Component({
  selector: 'app-inmo-editor',
  templateUrl: './inmo-editor.component.html',
  styleUrls: ['./inmo-editor.component.css']
})
export class InmoEditorComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private inmoService: InmoService,
    private router: Router ,
    private route: ActivatedRoute) { }

    list: InmoModel = {
    
      city:null,
      departament:null,
      value:null,
      address:null,
      property:null,
      offer: null
  
    }

  ngOnInit() {
    this.updateInmo();
    
  }

  updateInmo():void{
    let id = this.route.snapshot.params["_id"];
    this.inmoService.getInmoById(id).subscribe(item=>{
      this.list = item;
    });
  }
  updateNewInmo():void{
    this.inmoService.updateNewInmo(this.list).subscribe(item=>{
      openPlatformModalMessage("update!");
      this.router.navigate(['/home']);
    })
  }




  

}
