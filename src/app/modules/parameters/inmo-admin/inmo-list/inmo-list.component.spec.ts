import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InmoListComponent } from './inmo-list.component';

describe('InmoListComponent', () => {
  let component: InmoListComponent;
  let fixture: ComponentFixture<InmoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InmoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InmoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
