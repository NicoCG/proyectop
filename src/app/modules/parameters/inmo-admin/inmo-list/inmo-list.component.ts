import { Component, OnInit } from '@angular/core';
import { InmoService } from 'src/app/services/inmo.service';
import { InmoModel } from 'src/app/models/inmo.model';


@Component({
  selector: 'app-inmo-list',
  templateUrl: './inmo-list.component.html',
  styleUrls: ['./inmo-list.component.css']
})
export class InmoListComponent implements OnInit {

  constructor(private inmoService: InmoService ) { }

  inmoList: InmoModel[]=[];
  ngOnInit() {
    this.loadInmo();
  }

  loadInmo = () => {
    this.inmoService.loadAllInmo().subscribe(data =>{
     this.inmoList = data;
     console.log(data);
    });
 }

  

}
