import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';
import { AuthenticationRequiredGuard } from 'src/app/helpers/guards/authentication-required.guard';
import { UnathenticationRequiredGuard } from 'src/app/helpers/guards/unathentication-required.guard';
import { AsesorCreatorComponent } from './asesor-admin/asesor-creator/asesor-creator.component';
import { AsesorListComponent } from './asesor-admin/asesor-list/asesor-list.component';
import { AdminGuard } from 'src/app/helpers/guards/admin.guard';



const routes: Routes = [
  { 
    path:'login',
    component:LoginComponent,
    canActivate:[UnathenticationRequiredGuard]

  },
  {
    path:'logout',
    component:LogoutComponent,
    canActivate:[AuthenticationRequiredGuard]
    
  },
  {
    path:'register',
    component:RegisterComponent,
    canActivate:[UnathenticationRequiredGuard]

  },
  {
    path:'acreator',
    component:AsesorCreatorComponent,
    canActivate:[AuthenticationRequiredGuard]
    ,
    canLoad:[AdminGuard]
    

  },
  {
    path:'alist',
    component:AsesorListComponent,
    canActivate:[AuthenticationRequiredGuard],
    canLoad:[AdminGuard]

  },
 
  {
    path:'',
    pathMatch:'full',
    redirectTo:'/login'
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecurityRoutingModule { }
