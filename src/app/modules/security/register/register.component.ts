import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserModel } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

declare let openPlatformModalMessage: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  listu: UserModel = {
    rol: 3,
    firstname: null,
    lastname: null,
    email: null,
    phone: null,
    password: null
  }
  fgValidationRegister: FormGroup;
 
  constructor(private fb: FormBuilder, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.fgValidationBuilderRegister();
  }

  get fgr(){
    return this.fgValidationRegister.controls;
  }

  fgValidationBuilderRegister(){
    this.fgValidationRegister = this.fb.group({
      firstname:['',[Validators.required, Validators.minLength(4),Validators.maxLength(8)]],
      lastname:['',[Validators.required, Validators.minLength(4),Validators.maxLength(8)]],
      phone:['',[Validators.required,Validators.maxLength(10)]],
      email:['',[Validators.required, Validators.email]],
      password:['',[Validators.required, Validators.minLength(4),Validators.maxLength(8)]]
    })
  }

  saveUser():void {
    
    this.userService.saveNewUser(this.listu).subscribe(i=>{
      openPlatformModalMessage("completo");
      this.router.navigate(['/home']);
    })
   


    
  }
}

