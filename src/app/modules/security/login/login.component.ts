import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SecurityService } from "src/app/services/security.service";
import { Router } from "@angular/router";

declare var openPlatformModalMessage: any;

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  fgValidation: FormGroup;

  constructor(
    private fb: FormBuilder,
    private secService: SecurityService,
    private router: Router
  ) {}

  ngOnInit() {
    this.fgValidationBuilder();
  }

  fgValidationBuilder() {
    this.fgValidation = this.fb.group({
      username: [
        "pepito@gmail.com",
        [Validators.required, Validators.email, Validators.minLength(4)]
      ],
      password: [
        "12345",
        [Validators.required, Validators.minLength(4), Validators.maxLength(8)]
      ]
    });
  }

  loginEvent() {
    if (this.fgValidation.invalid) {
      alert("error data.");
    } else {
      let u = this.fg.username.value;
      let p = this.fg.password.value;
      let user = null;
      this.secService.loginUser(u, p).subscribe(data =>{
        if (data != null) {
          console.log(data);
          this.router.navigate(['/home']);
          this.secService.saveLoginInfo(data);
        } else {
          openPlatformModalMessage("The data is not valid!");
          
        }
      });
     
    }
  }

  get fg() {
    return this.fgValidation.controls;
  }

  
  

}
