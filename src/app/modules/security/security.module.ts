import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecurityRoutingModule } from './security-routing.module';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RegisterComponent } from './register/register.component';
import { AsesorCreatorComponent } from './asesor-admin/asesor-creator/asesor-creator.component';
import { AsesorListComponent } from './asesor-admin/asesor-list/asesor-list.component';



@NgModule({
  declarations: [LoginComponent, LogoutComponent, RegisterComponent, AsesorCreatorComponent, AsesorListComponent],
  imports: [
    CommonModule,
    SecurityRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SecurityModule { }
