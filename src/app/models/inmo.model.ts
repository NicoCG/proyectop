export class InmoModel{
    _id?:String;
    city: String;
    departament: String;
    address: String;
    value: number;
    property:String;
    offer: String;
    userId?:String;
}