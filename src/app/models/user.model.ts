import { AsesorModel } from './asesor.model';


export class UserModel{
    rol?:number;
    firstname: String;
    lastname: String;
    email: String;
    phone: number;
    password: String;
    isLogged?: boolean = false;
    isAsesor?:boolean = false;
    isClient?:boolean = false;
    isAdmin?:boolean = false;

}